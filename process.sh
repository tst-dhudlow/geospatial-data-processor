#!/bin/bash

PROCESSOR="$1"
INPUT="$2"
OUTPUT="$3"
EXTRA="$4"

ls node_modules &> /dev/null || npm install
node_modules/typescript/bin/tsc
node_modules/node/bin/node lib/process.js $PROCESSOR "$INPUT" "$OUTPUT" "$EXTRA"
