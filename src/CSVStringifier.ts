import csvStringify from 'csv-stringify/lib/sync';
import OutputStringifier from './OutputStringifier';

export default class CSVStringifier implements OutputStringifier {
  name = 'csv';
  stringify(output: string[]): string {
    return csvStringify(output);
  }
}