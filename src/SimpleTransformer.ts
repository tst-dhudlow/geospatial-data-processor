import Transformer from './Transformer';

export default class SimpleTransformer implements Transformer {
  public name = 'simple';
  private header: string[] | null = null;

  chunk(inputFiles: any[]): any[] {
    console.log("chunking...");
    console.log(inputFiles);
    let chunks: any[] = [];
    inputFiles.forEach((inputFile) => {
      if (!this.header) {
        this.header = inputFile.shift();
      } else {
        inputFile.shift();
      }
      chunks = chunks.concat(inputFile);
    });
    console.log("chunked...");
    console.log(chunks);
    return chunks;
  }

  transform(chunk: any): Promise<any> {
    console.log("transforming...");
    console.log(chunk);
    return new Promise((resolve) => {
      console.log("transformed!");
      chunk[1] += ", but better!"
      resolve(chunk);
    });
  }

  combine(results: any[]): any[] {
    return [
      [ this.header, results.shift(), results.shift() ],
      [ this.header ].concat(results)
    ];
  }
}