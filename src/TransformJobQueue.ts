import Transformer from './Transformer';

interface Job {
  id: number;
  input: any;
}

export default class TransformJobQueue {
  private transformer: Transformer;
  private waitingQueue: Job[];
  private pendingQueue: any;
  private results: any[];
  private resolve: ((result: any) => void) | null = null;
  private reject: ((error: any) => void) | null = null;
  private promise: Promise<any>;
  private MAX_PENDING: number = 10;

  constructor(transformer: Transformer, input: any) {
    this.transformer = transformer;
    this.waitingQueue = [];
    let inputs: any[] = this.transformer.chunk(input);
    inputs.forEach((input, index) => {
      this.waitingQueue.push({
        id: index,
        input
      });
    });
    this.pendingQueue = {};
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
    this.results = [];
  }

  transform(): Promise<any> {
    this.processQueue();
    return this.promise;
  }

  private processQueue() {
    console.log("Processing queue..." + this.waitingQueue.length + "/" + Object.keys(this.pendingQueue).length);
    if (this.waitingQueue.length === 0 && Object.keys(this.pendingQueue).length === 0) {
      console.log("Queues empty!");
      if (this.resolve) {
        console.log("Resolving...");
        return this.resolve(this.transformer.combine(this.results));
      } else {
        throw new Error('No promise set up to resolve upon queue processing completion!');
      }
    } else {
      while (Object.keys(this.pendingQueue).length < this.MAX_PENDING && this.waitingQueue.length > 0) {
        let job = this.waitingQueue.pop();
        console.log("Processing job...");
        console.log(job);
        if (job) {
          this.pendingQueue[job.id] = job;
          this.processJob(job);
        }
      }
    }
  }

  private processJob(job: Job) {
    this.transformer.transform(job.input).then((result) => {
      this.results[job.id] = result;
      delete this.pendingQueue[job.id];
      this.processQueue();
    }).catch((error) => {
      if (this.reject) {
        this.reject(error);
      }
    });

  }

}