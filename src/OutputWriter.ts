import fs from 'fs';

export default class OutputWriter {
  public static writeOutputs(outputs: string[], outputFiles: string[]) {
    for (let i = 0; i < outputs.length; i++) {
      fs.writeFileSync(outputFiles[i], outputs[i]);
    }
  }
}