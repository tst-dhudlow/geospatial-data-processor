import SimpleTransformer from './SimpleTransformer';
import AddressValidationTransformer from './AddressValidationTransformer';
import Transformer from './Transformer';

const simple = new SimpleTransformer();
const addressValidation = new AddressValidationTransformer();

let catalog: { [details: string]: Transformer }= {};
catalog[simple.name] = simple;
catalog[addressValidation.name] = addressValidation;

export default catalog;
