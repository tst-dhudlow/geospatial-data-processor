export default interface InputParser {
  name: string,
  parse: (input: string) => any;
}
