import TransformerCatalog from './TransformerCatalog';
import TransformJobQueue from './TransformJobQueue';
import InputReader from './InputReader';
import CSVParser from './CSVParser';
import CSVStringifier from './CSVStringifier';
import OutputWriter from './OutputWriter';

console.log(process.argv);

// Read arguments
const transformer: string = process.argv[2];
if (!TransformerCatalog[transformer]) {
  throw new Error("No transformer available by the name '" + transformer + "'!");
}
const inputFiles: string[] = process.argv[3].split(',');
const outputFiles: string[] = process.argv[4].split(',');

// Read input
let inputStrings = InputReader.readInputs(inputFiles);

// Parse
let inputs: any[] = [];
inputStrings.forEach((inputString) => {
  inputs.push(new CSVParser().parse(inputString));
});

// Transform
let queue = new TransformJobQueue(TransformerCatalog[transformer], inputs);
queue.transform().then((results: any[]) => {
  console.log(results);

  // Strigify
  let outputStrings: string[] = [];
  results.forEach((result) => {
    outputStrings.push(new CSVStringifier().stringify(result));
  });

  console.log("writing out to...");
  console.log(outputFiles);

  // Output
  OutputWriter.writeOutputs(outputStrings, outputFiles);
});


