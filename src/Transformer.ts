export default interface Transformer {
  name: string,
  chunk: (wholeInput: any) => any[];
  transform: (part: any) => Promise<any>;
  combine: (parts: any[]) => any;
}