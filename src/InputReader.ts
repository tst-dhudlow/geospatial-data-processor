
import fs from 'fs';

export default class InputReader {
  public static readInputs(inputFiles: string[]): string[] {
    let inputStrings: string[] = [];
    for (let i = 0; i < inputFiles.length; i++) {
      inputStrings.push(fs.readFileSync(inputFiles[i], 'utf8'));
    }
    return inputStrings;
  }
}