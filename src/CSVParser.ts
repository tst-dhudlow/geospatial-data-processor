import csvParse from 'csv-parse/lib/sync';
import InputParser from './InputParser';

export default class CSVParser implements InputParser {
  name = 'csv';

  parse(input: string): string[] {
    return csvParse(input);
  }
}
