import Transformer from './Transformer';
import axios from 'axios';

export default class SimpleTransformer implements Transformer {
  public name = 'addressValidation';
  private header: string[] | null = null;
  private key: string;
  private TARGET_TYPES: string[] = ["ROOFTOP", "RANGE_INTERPOLATED"];

  constructor() {
    this.key = process.argv[5];
    if (!this.key) {
      throw new Error("Address Validation transformer requires api key as fourth argument!");
    }
  }

  chunk(inputFiles: any[]): any[] {
    console.log("chunking...");
    console.log(inputFiles);
    let chunks: any[] = [];
    let inputFile = inputFiles[0];
    if (!this.header) {
      this.header = inputFile.shift();
    } else {
      inputFile.shift();
    }
    console.log("chunked...");
    console.log(chunks);
    return inputFile;
  }

  transform(chunk: any): Promise<any> {
    console.log("transforming...");
    console.log(chunk);
    return new Promise((resolve) => {
      let name = chunk[1];
      let addr1 = chunk[2];
      let addr2 = chunk[3];
      let addr3 = chunk[4];
      let addr4 = chunk[5];
      let city = chunk[6];
      let postal = chunk[7];
      let country = chunk[8];

      let address = addr1 + " " + addr2 + " " + addr3 + " " + addr4 + "," + city + " " + postal;
      let url = "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        encodeURIComponent(address) + (country ? "&components=country:" + country : "") +
        "&key=" + this.key;

      axios.get(url)
        .then(geo_response => {
          let results = geo_response.data && geo_response.data.results && geo_response.data.results;
          let result = results[0];
          let ltype = "";
          if (result) {
            ltype = result.geometry.location_type;
            for (var i = 1; results && i < results.length && !this.TARGET_TYPES.includes(ltype); i++) {
              console.log(results[i].geometry.location_type);
              if (this.TARGET_TYPES.includes(results[i].geometry.location_type)) {
                result = results[i];
                ltype = results[i].geometry.location_type;
              }
            }
            let standard_address = result.formatted_address;
            chunk.push(standard_address);
            chunk.push(result.geometry.location.lat + "," + result.geometry.location.lng);
          } else {
            chunk.push("NOT FOUND");
          }
          chunk.push(ltype);
          if (this.TARGET_TYPES.includes(ltype)) {
            chunk[14] = url;
            resolve(chunk);
          } else {
            let findplaceurl = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" +
              encodeURIComponent(name + ',' + addr1 + " " + addr2 + " " + addr3 + " " + addr4 + "," + city + "," + country) +
              "&inputtype=textquery&fields=formatted_address,geometry&key=" + this.key;
            console.log("FALLING BACK ON " + findplaceurl);
            axios.get(findplaceurl).then(place_response => {
              if (place_response.data.candidates && place_response.data.candidates.length) {
                let candidate = place_response.data.candidates[0]
                chunk.push(candidate.formatted_address);
                chunk.push(candidate.geometry.location.lat + "," + candidate.geometry.location.lng);
              }
              chunk[14] = url;
              chunk[15] = findplaceurl;
              resolve(chunk);
            }).catch(error => {
              console.error(error);
              chunk[14] = url;
              chunk[15] = findplaceurl;
              resolve(chunk);
            });

          }
        })
        .catch(error => {
          console.error(error);
          chunk[14] = url;
          resolve(chunk);
        });

    });
  }

  combine(results: any[]): any[] {
    if (this.header) {
      this.header.push('Geocode Standard Address');
      this.header.push('Latitude / Longitude');
      this.header.push('Location Type');
      this.header.push('Places Search Fallback');
      this.header.push('Places Search Latitude / Longitude');
      this.header.push('Geocode URL');
      this.header.push('Places Fallback URL');
      return [
        [ this.header ].concat(results)
      ];
    } else {
      throw new Error("Cannot rebuild CSV with no header!");
    }
  }
}