export default
interface OutputStringifier {
  name: string,
  stringify: (output: any) => string;
}
